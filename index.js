var vorpal = require('vorpal')();
var login = require('./login.js');
var s3Controller = require('./s3-controller.js');

var chalk = vorpal.chalk;

vorpal.delimiter('s3-tools$')
  .show();

vorpal.command('login', 'Logs in the user')
  .option('--phone <number>')
  .option('--password <string>')
  .action(function (args, callback) {
	login.loginWithPhone(args.options.phone, args.options.password)
      .then(function () {
	vorpal.log(chalk.green('Login successful'));
})
      .catch(function (err) {
	vorpal.log(chalk.red(err));
})
      .then(callback);
});

vorpal.command('get', 'Gets the data from the specified bucket and name')
  .option('--bucket <name>', 'Name of the s3 bucket. Defaults to the previous')
  .option('--name <name>', 'Name of the Object. Defaults to the previous')
  .action(function (args, callback) {
	s3Controller.get(String(args.options.bucket), String(args.options.name)).then(function (result) {
		vorpal.log(chalk.green(result.Body));
	}).catch(function (err) {
		vorpal.log(chalk.red(err));
	}).then(callback);
});

vorpal.command('put', 'Gets the data from the specified bucket and name')
  .option('--bucket <name>', 'Name of the s3 bucket. Defaults to the previous')
  .option('--name <name>', 'Name of the Object. Defaults to the previous')
  .action(function (args, callback) {
	s3Controller.put(String(args.options.bucket), String(args.options.name)).then(function (result) {
		vorpal.log(chalk.green('Object added.'));
	}).catch(function (err) {
		vorpal.log(chalk.red(err));
	}).then(callback);
});

vorpal.command('list', 'Gets the data from the specified bucket and name')
  .option('--bucket <name>', 'Name of the s3 bucket. Defaults to the previous')
  .option('--prefix <name>', 'Name of the Object. Defaults to the previous')
  .action(function (args, callback) {
	s3Controller.list(String(args.options.bucket), String(args.options.prefix)).then(function (result) {
		vorpal.log(chalk.green(JSON.stringify(result)));
	}).catch(function (err) {
		vorpal.log(chalk.red(err));
	}).then(callback);
});

vorpal.command('delete', 'Gets the data from the specified bucket and name')
  .option('--bucket <name>', 'Name of the s3 bucket. Defaults to the previous')
  .option('--name <name>', 'Name of the Object. Defaults to the previous')
  .action(function (args, callback) {
	s3Controller.delete(String(args.options.bucket), String(args.options.name)).then(function (result) {
		vorpal.log(chalk.green('Object deleted.'));
	}).catch(function (err) {
		vorpal.log(chalk.red(err));
	}).then(callback);
});
