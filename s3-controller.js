var https = require('https');
var axios = require('axios');
var aws = require('aws-sdk');
var q = require('q');
var config = require('./constants.json');
var login = require('./login.js');
var userData = require('./userdata.js');

module.exports = (function () {
	var credentials = {};
	var accessToken = null;
	var lastName = null;
	var lastBucket = null;
	var userId = null;
	var agent = new https.Agent({
		rejectUnauthorized: false
	});

	return {
		setAccessToken: setAccessToken,
		get: get,
		put: put,
		delete: remove,
		list: list
	};

	function setAccessToken(token) {
		accessToken = token;
	}

	function get(bucket, name) {
		var defer = q.defer();
		if (!name) {
			name = lastName;
		}
		if (!bucket) {
			bucket = lastBucket;
		}
		getCredentials().then(function (credentials) {
			var s3 = new aws.S3(credentials);
			s3.getObject({Bucket: bucket, Key: name}, function (err, data) {
				if (err) {
					defer.reject(err);
				} else {
					defer.resolve(data);
				}
			});
		});
		return defer.promise;
	}

	function put(bucket, name) {
		var defer = q.defer();
		if (!name) {
			name = lastName;
		}
		if (!bucket) {
			bucket = lastBucket;
		}

		getCredentials().then(function (credentials) {
			var s3 = new aws.S3(credentials);
			var setup = {Bucket: bucket, Key: name, Body: config.dataLoad};
			s3.putObject(setup, function (err, data) {
				if (err) {
					defer.reject(err);
				} else {
					defer.resolve(data);
				}
			});
		});
		return defer.promise;
	}

	function remove(bucket, name) {
		var defer = q.defer();
		if (!name) {
			name = lastName;
		}
		if (!bucket) {
			bucket = lastBucket;
		}

		getCredentials().then(function (credentials) {
			var s3 = new aws.S3(credentials);
			s3.deleteObject({Bucket: bucket, Key: name}, function (err, data) {
				if (err) {
					defer.reject(err);
				} else {
					defer.resolve(data);
				}
			});
		});
		return defer.promise;
	}

	function list(bucket, prefix) {
		var defer = q.defer();

		if (!bucket) {
			bucket = lastBucket;
		}

		getCredentials().then(function (credentials) {
			var s3 = new aws.S3(credentials);
			s3.listObjectsV2({Bucket: bucket, Prefix: prefix}, function (err, data) {
				if (err) {
					defer.reject(err);
				} else {
					defer.resolve(data);
				}
			});
		});
		return defer.promise;
	}

	function getCredentials() {
		var retVal;
		var expirDate = new Date(credentials.expiration);
		if (expirDate.getTime() > Date.now()) {
			retVal = q.resolve(credentials);
		} else if (accessToken) {
			retVal = axios.get(config.server + config.tokenEndpoint + '?token=' + accessToken, {httpsAgent: agent}).then(function (creds) {
				credentials.accessKeyId = creds.data.AccessKeyId;
				credentials.secretAccessKey = creds.data.SecretAccessKey;
				credentials.sessionToken = creds.data.SessionToken;
				credentials.apiVersion = '2006-03-01';
				credentials.signatureVersion = 'v4';
				return credentials;
			}).catch(function (err) {
				throw new Error('Failed at getting S3 credentials with: ' + err);
			});
		} else {
			retVal =	login.loginWithPhone().then(function (token) {
				accessToken = token;
				return getCredentials();
			});
		}

		return retVal;
	}
})();
