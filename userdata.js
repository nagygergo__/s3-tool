var https = require('https');
var axios = require('axios');
var q = require('q');
var login = require('./login.js');
var config = require('./constants.json');

module.exports = (function () {
	var userDataCache = null;
	var agent = new https.Agent({
		rejectUnauthorized: false
	});

	return {
		getUserData: getUserData
	};

	function getUserData() {
		var retVal;

		if (userDataCache) {
			retVal = q.resolve(userDataCache);
		} else {
			retVal = axios.get(config.server + config.userDataEndpoint + '?token=' + login.getSessionToken(), {
				httpsAgent: agent
			}).then(function (response) {
				userDataCache = response.data;
				return userDataCache;
			}).catch(function (err) {
				throw new Error('Could not log in. Reason: ' + err);
			});
		}
		return retVal;
	}
})();
