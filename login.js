var https = require('https');
var axios = require('axios');
var config = require('./constants.json');

module.exports = (function () {
	var cachedUser = {};
	var sessionToken = null;

	var agent = new https.Agent({
		rejectUnauthorized: false
	});

	return {
		loginWithPhone: loginWithPhone,
		getSessionToken: getSessionToken
	};

	function loginWithPhone(phone, pass) {
		if (phone && pass) {
			cachedUser.phone = phone;
			cachedUser.password = pass;
		}
		var reqBody = {
			phone: String(cachedUser.phone),
			password: cachedUser.password
		};

		return axios.post(config.server + config.loginEndpoint, reqBody, {
			httpsAgent: agent
		}).then(function (response) {
			var sessionToken = response.data.token;
			return sessionToken;
		}).catch(function (err) {
			throw new Error('Could not log in. Reason: ' + err);
		});
	}

	function getSessionToken() {
		return sessionToken;
	}
})();
